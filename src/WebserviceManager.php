<?php
namespace Riddlemd\Webservice;

abstract class WebserviceManager
{
    use \Cake\Core\StaticConfigTrait {
        config as protected _config;
        parseDsn as protected _parseDsn;
    }

    protected static $_webservices = [];

    public static function get(string $name)
    {
        if(empty(static::$_config[$name])) { throw new \Exception("Missing Webservice Config [{$name}]"); }

        if(!isset(static::$_webservices[$name]))
        {
            $className = static::getConfig($name)['className'] ?? null;
            if(empty($className)) { throw new \Exception("className for '{$name}' NOT specified!"); }

            $webservice = new $className(static::getConfig($name));
            if(!is_a($webservice, '\Riddlemd\Webservice\Webservice')) { throw new \Exception('Webservice must be inherit from Riddlemd\Webservice\Webservice'); }
            
            static::$_webservices[$name] = $webservice;
        }

        return static::$_webservices[$name];
    }
}