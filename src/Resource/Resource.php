<?php
namespace Riddlemd\Webservice\Resource;

use Cake\ORM\Entity;

class Resource extends Entity
{
    protected $_isPartial = false;
    public function isPartial() : bool
    {
        return $this->_isPartial;
    }

    public function setPartial(bool $bool)
    {
        $this->_isPartial = $bool;
        return $this;
    }

    public function __debugInfo()
    {
        $debugInfo = parent::__debugInfo();
        $debugInfo['[partial]'] = $this->_isPartial;

        return $debugInfo;
    }
}