<?php
namespace Riddlemd\Webservice;

use Cake\Http\Client;
use Cake\Core\Configure;

class Webservice
{
    use \Cake\Core\InstanceConfigTrait;

    protected $_defaultConfig = [
        'scheme' => 'http',
        'host' => '',
        'port' => 80
    ];
    private $_client;

    public function __construct(array $config = [])
    {
        $this->setConfig($config);

        $this->initialize();
    }

    public function initialize()
    {
        $this->setClient(new Client([
            'scheme' => $this->getConfig('scheme'),
            'host' => $this->getConfig('host'),
            'port' => $this->getConfig('port')
        ]));
    }

    protected function getClient() : Client
    {
        return $this->_client;
    }

    protected function setClient(Client $client)
    {
        $this->_client = $client;
    }

    protected function request(string $path, array $data = [], array $options = [], string $method = 'get')
    {
        $response = $this->getClient()->$method($path, $data, $options);
        return $response;
    }

    public function get(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'get');
    }

    public function head(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'head');
    }

    public function options(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'options');
    }

    public function patch(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'patch');
    }

    public function post(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'post');
    }

    public function put(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'put');
    }

    public function trace(string $path, array $data = [], array $options = [])
    {
        return $this->request($path, $data, $options, 'trace');
    }
}