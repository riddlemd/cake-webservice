<?php
namespace Riddlemd\Webservice\Endpoint;

use Riddlemd\Webservice\WebserviceManager;
use Riddlemd\Webservice\Resource\Resource;
use Cake\Utility\Inflector;

class Endpoint
{
    use \Cake\Core\InstanceConfigTrait;

    protected $_defaultConfig = [
        'resourceClassName' => '\Riddlemd\Webservice\Resource\Resource',
        'savePartialResources' => false,
    ];
    private $_webservice;

    public function __construct(array $config = [])
    {
        $thisClass = get_class($this);
        $namespace = implode('\\', array_slice(explode('\\', $thisClass), 0, -2));
        $name = array_slice(explode('\\', $thisClass), -1, 1)[0];
        $name = Inflector::singularize(substr($name, 0, strlen($name) - 8));
        $resourceClassName = "\\{$namespace}\\Resource\\{$name}";

        if(class_exists($resourceClassName)) $this->_defaultConfig['resourceClassName'] = $resourceClassName;

        $this->setConfig($config);

        $this->initialize();
    }

    public function initialize()
    {
        if($this->getConfig('webservice'))
        {
            $this->setWebservice($this->getConfig('webservice'));
        }
    }

    protected function setWebservice(string $name)
    {
        $this->_webservice = WebserviceManager::get($name);
    }
    
    public function getWebservice()
    {
        return $this->_webservice;
    }

    public function newResource()
    {
        $className = $this->getConfig('resourceClassName');
        $resource = new $className;
        $resource->setSource(get_class($this));
        return $resource;
    }

    public function patchResource(Resource $resource, array $data, array $options = [])
    {
        if(!is_a($resource, $this->getConfig('resourceClassName'))) { throw new \Exception("Resource not instance of '{$this->getConfig('resourceClassName')}'"); }

        foreach($data as $key => $value)
        {
            $resource->$key = $value;
        }

        return $resource;
    }

    public function find(array $options = [])
    {
        return [];
    }

    public function get(string $primaryKey, array $options = [])
    {
        return null;
    }

    public function save(Resource $resource)
    {
        if(!is_a($resource, $this->getConfig('resourceClassName'))) { throw new \Exception("Resource not instance of '{$this->getConfig('resourceClassName')}'"); }
        if($resource->isPartial() && !$this->getConfig('savePartialResources')) { throw new \Exception("Webservice does not support saving partial resources'"); }
        return false;
    }
}