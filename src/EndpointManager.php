<?php
namespace Riddlemd\Webservice;

abstract class EndpointManager
{
    use \Cake\Core\StaticConfigTrait {
        config as protected _config;
        parseDsn as protected _parseDsn;
    }

    protected static $_endpoints = [];

    public static function get(string $name)
    {
        if(!isset(static::$_endpoints[$name]))
        {
            [$plugin, $class] = pluginSplit($name);
            $plugin = str_replace('/', '\\', $plugin);
            $class = "\\{$plugin}\\Webservice\Endpoint\\{$class}Endpoint";
            static::$_endpoints[$name] = new $class;
        }

        return static::$_endpoints[$name];
    }
}